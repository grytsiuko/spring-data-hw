package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findByLastNameContainingIgnoreCase(String lastName, Pageable pageable);

    @Query("SELECT u " +
            "FROM User u " +
            "WHERE u.office.city = :city " +
            "ORDER BY u.lastName")
    List<User> findByOfficeCityOrderByLastName(String city);

    List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(Integer experience);

    @Query("SELECT u " +
            "FROM User u " +
            "WHERE u.office.city = :city AND " +
            "      u.team.room = :room")
    List<User> findByRoomAndCity(String room, String city, Sort sort);

    @Transactional
    @Modifying
    @Query("DELETE " +
            "FROM User u " +
            "WHERE u.experience < :experience")
    Integer deleteByExperienceLessThan(Integer experience);
}
