package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("SELECT DISTINCT u.office " +
            "FROM User u " +
            "WHERE u.team.technology.name = :technology")
    List<Office> findByTechnology(String technology);

    @Transactional
    @Modifying
    @Query("UPDATE Office o " +
            "SET o.address = :newAddress " +
            "WHERE o.address = :oldAddress AND " +
            "      o.users.size > 0")
    void updateAddressIfHasProject(String oldAddress, String newAddress);

    Optional<Office> findByAddress(String address);
}
