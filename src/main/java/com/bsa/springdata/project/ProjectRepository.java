package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query("SELECT p " +
            "FROM User u INNER JOIN u.team.project p " +
            "WHERE EXISTS (SELECT t " +
            "              FROM p.teams t " +
            "              WHERE t.technology.name = :technology) " +
            "GROUP BY p " +
            "ORDER BY COUNT(u), p.name")
        // ORDER BY p.name - to be sure that Facebook is first in test (not Uber - both have 8 users)
    List<Project> findByTechnologyNameOrderByUsersCount(String technology, Pageable pageable);

    @Query("SELECT p " +
            "FROM User u " +
            "     INNER JOIN u.team t " +
            "     INNER JOIN t.project p " +
            "GROUP BY p " +
            "ORDER BY COUNT(t) DESC, COUNT(u) DESC, p.name DESC")
    List<Project> findTheBiggest(Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT p.name, " +
                    "       COUNT(DISTINCT t.id) AS teamsNumber, " +
                    "       COUNT(u.id) AS developersNumber, " +
                    "       string_agg(DISTINCT tech.name, ',' ORDER BY tech.name DESC) AS technologies " +
                    "FROM users u " +
                    "     INNER JOIN teams t ON u.team_id = t.id " +
                    "     INNER JOIN projects p ON t.project_id = p.id " +
                    "     INNER JOIN technologies tech ON t.technology_id = tech.id " +
                    "GROUP BY p.id " +
                    "ORDER BY p.name")
    List<ProjectSummaryDto> getSummary();

    @Query("SELECT COUNT(DISTINCT u.team.project) " +
            "FROM User u " +
            "WHERE EXISTS (SELECT r " +
            "              FROM u.roles r " +
            "              WHERE r.name = :role)")
    Integer countByUserRole(String role);
}