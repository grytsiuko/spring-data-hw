package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        var oldTechnology = technologyRepository.findByName(oldTechnologyName);
        var newTechnology = technologyRepository.findByName(newTechnologyName);

        if (oldTechnology.isPresent() && newTechnology.isPresent()) {
            teamRepository.updateTechnologyByUsersCountLessThan(
                    devsNumber, oldTechnology.get(), newTechnology.get()
            );
        }
    }

    public void normalizeName(String name) {
        teamRepository.normalizeName(name);
    }
}
