package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    Optional<Team> findByName(String name);

    @Query("SELECT COUNT(t) " +
            "FROM Team t " +
            "WHERE t.technology.name = :technology")
    Integer countByTechnologyName(String technology);

    @Transactional
    @Modifying
    @Query("UPDATE Team t " +
            "SET t.technology = :newTechnology " +
            "WHERE t.technology = :oldTechnology AND " +
            "      t.users.size < :devsNumber ")
    void updateTechnologyByUsersCountLessThan(int devsNumber, Technology oldTechnology, Technology newTechnology);

    @Transactional
    @Modifying
    @Query(nativeQuery = true,
            value = "UPDATE teams t " +
                    "SET name = concat(t.name, '_', p.name, '_', tech.name) " +
                    "FROM projects p, technologies tech " +
                    "WHERE t.name = :name AND " +
                    "      t.project_id = p.id AND " +
                    "      t.technology_id = tech.id")
    void normalizeName(String name);
}
