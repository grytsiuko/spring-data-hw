package com.bsa.springdata.team;

import com.bsa.springdata.project.Project;
import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.user.User;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "teams")
public class Team {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private String name;

    @Column
    private String room;

    @Column
    private String area;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    @OneToMany(mappedBy = "team", fetch = FetchType.LAZY)
    private List<User> users;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "technology_id")
    private Technology technology;

    public static Team fromProjectDto(CreateProjectRequestDto createProjectRequest,
                                      Project project,
                                      Technology technology) {
        return Team
                .builder()
                .area(createProjectRequest.getTeamArea())
                .name(createProjectRequest.getTeamName())
                .room(createProjectRequest.getTeamRoom())
                .project(project)
                .technology(technology)
                .build();
    }
}
