package com.bsa.springdata.team;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "technologies")
public class Technology {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String link;

    public static Technology fromProjectDto(CreateProjectRequestDto createProjectRequest) {
        return Technology
                .builder()
                .name(createProjectRequest.getTech())
                .link(createProjectRequest.getTechLink())
                .description(createProjectRequest.getTechDescription())
                .build();
    }
}